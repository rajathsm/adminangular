// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAzUUFiM2VJgiwzk_xI3wKqLiYMO5dnb38",
    authDomain: "testing-cd327.firebaseapp.com",
    databaseURL: "https://testing-cd327.firebaseio.com",
    projectId: "testing-cd327",
    storageBucket: "testing-cd327.appspot.com",
    messagingSenderId: "619259273830",
    appId: "1:619259273830:web:caa5d44e68f896d86b03a7",
    measurementId: "G-VXJL6H7PYN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
