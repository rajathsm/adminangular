import { Component, OnInit } from '@angular/core';
import {Client} from 'src/app/models/Clients';
import { HttpClient } from '@angular/common/http'
import {ClientService} from 'src/app/services/client.service';
import { AngularFireDatabase,AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import 'firebase/database';
import { slot } from '../../models/slots';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  [x: string]: any;
  baseUrl='http://localhost:5000';

  //clients:Client[];
  totalOwed:number;
  clients:any[];
  clients2New2;//:any[];
  item1: Observable<any>;
  listref:Observable<any>;
  
  gh: Observable<any[]>;
  gl:string="https://testing-cd327.firebaseio.com/";
  items: AngularFireList<slot>;
  value:number;




  itemRef: AngularFireObject<any>;
  item: Observable<any>;

  //firebase rules : allow read, write: if request.auth != null;
  

  constructor(public clientService:ClientService,private httpClient: HttpClient,db: AngularFireDatabase) {
    // this.item1 = db.list('testing-cd327').stateChanges();
    // console.log("realtimeDatabse");
    // const listRef = db.list('slot_1');

    // //this.item1.push({ name: newName });
    // // this.item1=this.gh;
    // console.log(this.item1);
    // console.log("#############")
    // console.log(listRef);
    // // console.log(this.gh);



    // this.itemRef = db.object('slot_1');
    // this.item = this.itemRef.valueChanges();


    this.itemRef = db.object('slot_1');
this.itemRef.snapshotChanges().subscribe(action => {
  console.log(action.type);
  console.log(action.key)
  console.log(action.payload.val())

  this.value=action.payload.val();
});


   }

  ngOnInit() {

    this.clientService.getClients().subscribe(clients => {
      this.clients = clients;
      console.log(this.clients);
      this.getTotalOwed();
    });

    this.clientService.getVehicle().subscribe(clients2New2 => {
      this.clients2New2 = clients2New2;
      console.log(this.clients2New2);
      
    });

    console.log(this.clientService.createdId())
  }



  get_products(){
    this.httpClient.get(this.baseUrl + '/time').subscribe((res)=>{
        console.log(res);
    });
}

get_products3(){
  this.httpClient.post('http://localhost:5000/time','msg').subscribe((res)=>{
      console.log(res);
  });
}



  getTotalOwed(){
    // let total = 0;
    // for(let i = 0;i < this.clients2New2[0].length;i++){
    //   total += parseFloat(this.clients2New2.parkingFees);
    // }
    // this.totalOwed = total;
    // console.log(this.totalOwed);
  }

}
