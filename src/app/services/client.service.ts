import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument,AngularFirestoreCollection } from '@angular/fire/firestore';
import {Client} from 'src/app/models/Clients';
import { Observable } from 'rxjs';
import {Clients2New} from 'src/app/models/Clients2';



@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private AdminCollection: AngularFirestoreCollection<Client>;
  private vehicleCollection: AngularFirestoreCollection<Clients2New>;

  clients:Observable<Client[]>;
  clients2:Observable<any[]>;
  clients2New:Observable<Clients2New[]>;
  //client:Observable<any>;

  constructor(public af:AngularFirestore) { 
    this.AdminCollection = af.collection<Client>('Admin');
    //this.clients = this.AdminCollection.valueChanges();
    this.clients2=this.AdminCollection.valueChanges();


    this.vehicleCollection = af.collection<Clients2New>('vehicle');
    //this.clients = this.AdminCollection.valueChanges();
    this.clients2New=this.vehicleCollection.valueChanges();
    //console.log(this.clients2New);

    

  
  }

  getClients(){
    return this.clients2;
  }
  getVehicle(){
    return this.clients2New;
  }

  createdId(){
    var idBefore =  this.af.createId();
    return idBefore;
  }
}
